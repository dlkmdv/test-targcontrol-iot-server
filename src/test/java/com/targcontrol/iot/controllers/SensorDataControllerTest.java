package com.targcontrol.iot.controllers;

import com.targcontrol.iot.views.SensorDataViewModel;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
class SensorDataControllerTest {

    private static final String API_ROOT = "http://localhost:8080/api/v1";

    private static final UUID ORG_ID = UUID.fromString("ccedd313-7351-4fcc-873a-d6dbf629c67c");
    private static final UUID DEV_ID = UUID.fromString("84751ef9-627a-4d11-b5dd-1c5884702228");
    private static final UUID SENSOR_ID = UUID.fromString("6c790bd0-c99a-48ad-8df0-edc82bb7222a");

    private static final ZonedDateTime START_DT = ZonedDateTime.parse("2020-03-01T00:00:00+03:00");
    private static final ZonedDateTime END_DT = ZonedDateTime.parse("2020-04-01T00:00:00+03:00");

    private ZonedDateTime between(ZonedDateTime startInclusive) {
        int mins = (new Random()).nextInt(31 * 24 * 60);
        return startInclusive.plusMinutes(mins);
    }

    private SensorDataViewModel create() {
        SensorDataViewModel result = new SensorDataViewModel();

        result.setId(UUID.randomUUID());
        result.setOrganizationId(ORG_ID);
        result.setDeviceId(DEV_ID);
        result.setSensorId(SENSOR_ID);
        result.setValue((new Random()).nextDouble());
        result.setDateTime(this.between(START_DT));


        return result;
    }

/*    @Test
    public void whenInvalidRequest_thenError() {
        Response response = RestAssured.get(API_ROOT + "/sensor-data");
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    }*/

    @Test
    public void whenSync_thenOK() {
        List<SensorDataViewModel> data = IntStream.range(0, 100).mapToObj(x -> this.create()).collect(Collectors.toList());

        Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(data)
                .post(API_ROOT + "/sync");

        assertEquals(HttpStatus.NO_CONTENT.value(), response.getStatusCode());
    }
}