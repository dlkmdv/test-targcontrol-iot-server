package com.targcontrol.iot.controllers;

import com.targcontrol.iot.models.Role;
import com.targcontrol.iot.views.UserRequest;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Sql(scripts = {"classpath:user_data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = {"classpath:delete_from_users_table.sql"}, executionPhase = AFTER_TEST_METHOD)
public class UserControllerTest {

    @LocalServerPort
    private int port;

    private static final String API_ROOT = "/api/v1";

    private static final String AUTH_USER_PASSWORD = "pass";
    private static final String AUTH_USER_USERNAME = "auth_username";
    private static final UUID AUTH_USER_ID = UUID.fromString("b26ba302-db7e-476f-bfc8-c93e786250b0");
    private static final UUID AUTH_USER_ORG_ID = UUID.fromString("ccedd313-7351-4fcc-873a-d6dbf629c67c");
    private static final String AUTH_USER_EMAIL = "test.email@gmail.com";
    private static final Boolean AUTH_USER_IS_ENABLE = true;
    private static final Role AUTH_USER_ROLE = Role.ROLE_USER;

    private static final String AUTH_ADMIN_PASSWORD = "pass";
    private static final String AUTH_ADMIN_USERNAME = "auth_admin_username";

    private static final UUID USER_ID_FOR_SAVE = UUID.fromString("d7e457d5-08c5-4526-b6bc-7ceef8ad2a67");


    @Test
    public void whenSave_thenOK() {
        UserRequest data = create();

        data.setUsername("test_username");
        data.setId(USER_ID_FOR_SAVE);
        data.setOrganizationId(UUID.randomUUID());

        Response response = RestAssured.given()
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(data)
                .auth()
                .basic(AUTH_ADMIN_USERNAME, AUTH_ADMIN_PASSWORD)
                .post(createURLWithPort("/user/save"));

        assertEquals(HttpStatus.CREATED.value(), response.getStatusCode());
    }

    @Test
    public void whenUpdate_thenOk() {
        UserRequest data = create();
        data.setEmail("newemail11@gmail.com");
        Response response = RestAssured.given()
                .auth()
                .basic(AUTH_USER_USERNAME, AUTH_USER_PASSWORD)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(data)
                .put(createURLWithPort("/user/update/" + data.getId()));

        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    }

    @Test
    public void whenGetById_thenOk() {
        Response response = RestAssured.given()
                .auth()
                .basic(AUTH_USER_USERNAME, AUTH_USER_PASSWORD)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .get(createURLWithPort("/user/" + AUTH_USER_ID));

        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertNotNull(response.body());
    }

    @Test
    public void whenGetById_thenNotFound() {
        Response response = RestAssured.given()
                .auth()
                .basic(AUTH_USER_USERNAME, AUTH_USER_PASSWORD)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .get(createURLWithPort("/user/" + UUID.randomUUID()));

        assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
    }

    @Test
    public void whenDelete_thenNoContent() {
        UserRequest userRequest = create();
        Response response = RestAssured.given()
                .auth()
                .basic(AUTH_ADMIN_USERNAME, AUTH_ADMIN_PASSWORD)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(userRequest)
                .delete(createURLWithPort("/user/" + userRequest.getId()));

        assertEquals(HttpStatus.NO_CONTENT.value(), response.getStatusCode());
    }

    @Test
    public void whenFindAll_thenOk() {
        UserRequest userRequest = create();
        Response response = RestAssured.given()
                .auth()
                .basic(AUTH_ADMIN_USERNAME, AUTH_ADMIN_PASSWORD)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(userRequest)
                .get(createURLWithPort("/user/all"));

        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    }


    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + API_ROOT + uri;
    }

    private UserRequest create() {
        UserRequest result = new UserRequest();
        result.setId(AUTH_USER_ID);
        result.setEmail(AUTH_USER_EMAIL);
        result.setUsername(AUTH_USER_USERNAME);
        result.setEnable(AUTH_USER_IS_ENABLE);
        result.setRole(AUTH_USER_ROLE);
        result.setOrganizationId(AUTH_USER_ORG_ID);
        result.setPassword(AUTH_USER_PASSWORD);
        return result;
    }
}
