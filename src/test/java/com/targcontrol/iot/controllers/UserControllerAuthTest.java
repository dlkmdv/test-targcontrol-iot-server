package com.targcontrol.iot.controllers;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Sql(scripts = {"classpath:user_data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(scripts = {"classpath:delete_from_users_table.sql"}, executionPhase = AFTER_TEST_METHOD)
public class UserControllerAuthTest {

    @LocalServerPort
    private int port;

    private static final String API_ROOT = "/api/v1";
    private static final String AUTH_USER_PASSWORD = "pass";
    private static final String AUTH_USER_USERNAME = "auth_username";

    @Test
    public void whenFindAll_thenUnauthorized() {
        Response response = RestAssured.given()
                .auth()
                .basic("wrong_username", "wrong_pass")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .get(createURLWithPort("/user/all"));

        assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatusCode());
    }

    @Test
    public void whenFindAll_thenForbidden() {
        Response response = RestAssured.given()
                .auth()
                .basic(AUTH_USER_USERNAME, AUTH_USER_PASSWORD)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .get(createURLWithPort("/user/all"));

        assertEquals(HttpStatus.FORBIDDEN.value(), response.getStatusCode());
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + API_ROOT + uri;
    }

}