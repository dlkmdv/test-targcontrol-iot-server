package com.targcontrol.iot.controllers;

import com.targcontrol.iot.models.DeviceState;
import com.targcontrol.iot.views.DeviceRequest;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD;
import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.BEFORE_TEST_METHOD;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)

//@Sql(scripts = {"classpath:clear_device_table.sql"}, executionPhase = BEFORE_TEST_METHOD)
@Sql(scripts = {"classpath:device_data.sql"}, executionPhase = BEFORE_TEST_METHOD)
@Sql(scripts = {"classpath:clear_device_table.sql"}, executionPhase = AFTER_TEST_METHOD)
class DeviceControllerValidationTest {

  ConrtollerTestUtil conrtollerTestUtil = new ConrtollerTestUtil();

  @LocalServerPort
  private int port;

  private static final String API_ROOT = "/api/v1";

  private static final String ID_RAND_STR = "3dc11fa3-8a59-4d7f-9f73-6939c1fb3752";
  private static final String ID_ANOTHERS_STR = "d6f10ec9-fb7b-4596-8b5c-808da6220320";
  private static final UUID ID_ANOTHERS = UUID.fromString(ID_ANOTHERS_STR);
  private static final String NAME_DEL = "deviceOne";


  public String createURLWithPort(String uri) {
    return "http://localhost:" + port + API_ROOT + uri;
  }

  @Test
  void whenUpdate_thenNotFound() {
    DeviceRequest data = conrtollerTestUtil
        .create(Optional.of(ID_ANOTHERS), NAME_DEL, DeviceState.DEVICE_ACTIVE,
            Optional.empty());
    Response response = RestAssured.given()
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(data)
        .put(createURLWithPort("/device/update"));
    assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
  }

  @ParameterizedTest
  @ValueSource(strings = {"name", "state"})
  void whenUpdate_thenBadRequest(String missingValue) {
    DeviceRequest data;
    switch (missingValue) {
      case ("name"):
        data = conrtollerTestUtil
            .create(Optional.of(ID_ANOTHERS), null, DeviceState.DEVICE_ACTIVE,
                Optional.empty());
        break;
      case ("state"):
        data = conrtollerTestUtil
            .create(Optional.of(ID_ANOTHERS), NAME_DEL, null,
                Optional.empty());
        break;
      default:
        data = new DeviceRequest();
    }
    Response response = RestAssured.given()
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(data)
        .put(createURLWithPort("/device/update"));
    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCode());
  }


  @Test
  void whenDelete_thenNotFound() {
    DeviceRequest data = conrtollerTestUtil
        .create(Optional.of(ID_ANOTHERS), NAME_DEL, DeviceState.DEVICE_ACTIVE,
            Optional.empty());
    Response response = RestAssured.given()
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(data)
        .post(createURLWithPort("/device/delete"));
    assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
  }

  @ParameterizedTest
  @ValueSource(strings = {"name", "state"})
  void whenDelete_thenBadRequest(String missingValue) {
    DeviceRequest data;
    switch (missingValue) {
      case ("name"):
        data = conrtollerTestUtil
            .create(Optional.of(ID_ANOTHERS), null, DeviceState.DEVICE_ACTIVE,
                Optional.empty());
        break;
      case ("state"):
        data = conrtollerTestUtil
            .create(Optional.of(ID_ANOTHERS), NAME_DEL, null,
                Optional.empty());
        break;
      default:
        data = new DeviceRequest();
    }
    Response response = RestAssured.given()
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .body(data)
        .post(createURLWithPort("/device/delete"));
    assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatusCode());
  }

  @ParameterizedTest
  @ValueSource(strings = {ID_ANOTHERS_STR, ID_RAND_STR})
  void whenGetById_thenNotFound(String id) {
    Response response = RestAssured.given()
        .contentType(MediaType.APPLICATION_JSON_VALUE)
        .get(createURLWithPort("/device/" + id));
    assertEquals(HttpStatus.NOT_FOUND.value(), response.getStatusCode());
    assertNotNull(response.body());
  }


}
