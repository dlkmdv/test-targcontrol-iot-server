insert into targcontrol_iot.device (id, organization_id, name, token,device_state)
values('d6f10ec9-fb7b-4596-8b5c-808da6220320', '33d9720a-1642-4a54-ab89-57b8c7c5ebd4', 'deviceOne','3f9a5cc4-8e78-4557-a104-abeb827e1491', 'DEVICE_ACTIVE');

insert into targcontrol_iot.device (id, organization_id, name, token,device_state)
values('3a9b202d-8982-4ec0-b610-d5e1bcabafa0', '8f8e70ec-eb38-461b-807c-5133ec637f2f', 'deviceTwo','c7501915-5dab-49ee-b441-6a9869e5ff5e', 'DEVICE_ACTIVE');

insert into targcontrol_iot.device (id, organization_id, name, token,device_state)
values('d07c2bf1-8cb3-40fe-b61a-5f9ccc41e774', '8f8e70ec-eb38-461b-807c-5133ec637f2f', 'deviceTree', 'f96978b5-90d9-4aa2-aa6d-85844362355e', 'DEVICE_DELETED');

insert into targcontrol_iot.device (id, organization_id, name, token, device_state)
values('a2c816b9-7560-4066-8813-0b50bd646a21', '8f8e70ec-eb38-461b-807c-5133ec637f2f', 'deviceFour', 'db7db901-763c-426b-8a64-343d82d1fcfd','DEVICE_ACTIVE');

insert into targcontrol_iot.sensor (id, organization_id, name, device_id)
values('cd4ac4ea-83ad-4e3e-8b49-b31efb6d09d4', '8f8e70ec-eb38-461b-807c-5133ec637f2f', 'sens1', '3a9b202d-8982-4ec0-b610-d5e1bcabafa0');

insert into targcontrol_iot.sensor (id, organization_id, name, device_id)
values('b59b2efe-3c99-4951-a740-2bb7181660aa', '8f8e70ec-eb38-461b-807c-5133ec637f2f', 'sens2', 'd07c2bf1-8cb3-40fe-b61a-5f9ccc41e774');

insert into targcontrol_iot.sensor (id, organization_id, name, device_id)
values('c10066cd-19d5-4ba6-ba85-fe18d453f9a0', '33d9720a-1642-4a54-ab89-57b8c7c5ebd4', 'sens1', 'd6f10ec9-fb7b-4596-8b5c-808da6220320');

