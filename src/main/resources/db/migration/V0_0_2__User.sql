CREATE TABLE targcontrol_iot.user (
  id                UUID DEFAULT uuid_generate_v4(),
  username          TEXT NOT NULL,
  pass              TEXT NOT NULL,
  email             TEXT NOT NULL,
  organization_id   UUID NOT NULL,
  role              TEXT NOT NULL,
  enable            BOOLEAN NOT NULL,
  CONSTRAINT uk_user UNIQUE (username,organization_id),
  CONSTRAINT pk_user PRIMARY KEY (id)
);

INSERT INTO targcontrol_iot.user VALUES ('ccedd313-7351-4fcc-873a-d6dbf629c67c','sa@targiot.com','$2a$10$IXW5kvUPagqYS0ZN9Eo3r.viATyrqZrX.HzhKfjBE.MzvdE7Tw2gy','sa@targiot.com','a30e4fc9-1d07-4ef0-80b3-44ed2768d0fb','ROLE_ADMIN', true);