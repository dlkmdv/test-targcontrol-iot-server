package com.targcontrol.iot.exceptions;

import java.util.UUID;

public class DeviceNotFoundException extends RuntimeException {

  private static final String DEVICE_NOT_FOUND_EXCEPTION_MESSAGE = "Device with specified Id = %s not found";

  public DeviceNotFoundException(UUID id) {
    super(String.format(DEVICE_NOT_FOUND_EXCEPTION_MESSAGE, id));
  }

}
