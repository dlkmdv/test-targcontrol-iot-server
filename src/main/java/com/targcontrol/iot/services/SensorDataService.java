package com.targcontrol.iot.services;

import com.targcontrol.iot.models.SensorData;
import com.targcontrol.iot.repositories.SensorDataRepository;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class SensorDataService {

    private final SensorDataRepository sensorDataRepository;

    public List<SensorData> findAll(UUID organizationId, ZonedDateTime startDate, ZonedDateTime endDate) {
        List<SensorData> data = this.sensorDataRepository
                .findAllByOrganizationIdAndDateTimeGreaterThanEqualAndDateTimeLessThanEqual(organizationId, startDate, endDate);
        return this.process(data);
    }

    public List<SensorData> save(List<SensorData> data) {
        return this.sensorDataRepository.saveAll(data);
    }


    private List<SensorData> process(List<SensorData> data) {
        double threshold = 1000.0;
        double min = 0.0;
        double max = 1.0;

        if (data.size() < 2) {
            return new ArrayList<>();
        }

        //threshold filter
        data = data.parallelStream()
                .peek(x -> x.setValue((x.getValue() > threshold) ? max : min))
                .collect(Collectors.toList());


        List<SensorData> result = new ArrayList<>();

        for (int i = 0; i < data.size(); i++) {

            if (i == 0) {
                result.add(data.get(i));
                continue;
            }

            if (i == data.size() - 1) {
                result.add(data.get(data.size() - 1));
                continue;
            }

            double current = data.get(i).getValue();
            double next = data.get(i + 1).getValue();

            if (current != next) {
                result.add(data.get(i));
                result.add(data.get(i + 1));
            }
        }

        return result;
    }
}
