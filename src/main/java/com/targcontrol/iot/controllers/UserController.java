package com.targcontrol.iot.controllers;

import com.targcontrol.iot.exceptions.UserNotFoundException;
import com.targcontrol.iot.mappers.UserMapper;
import com.targcontrol.iot.services.UserService;
import com.targcontrol.iot.views.UserRequest;
import com.targcontrol.iot.views.UserViewModel;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/user")
public class UserController {

  private final UserService userService;
  private final UserMapper userMapper;

  @PreAuthorize("hasRole('ADMIN')")
  @GetMapping("/all")
  public List<UserViewModel> findAll() {
    return userMapper.toDto(userService.findAll());
  }

  @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
  @GetMapping("/{id}")
  public UserViewModel findById(@PathVariable UUID id) throws UserNotFoundException {
    return userMapper
        .toDto(userService.findById(id).orElseThrow(() -> new UserNotFoundException(id)));
  }

  @PreAuthorize("hasRole('ADMIN')")
  @PostMapping("/save")
  @ResponseStatus(HttpStatus.CREATED)
  public UserViewModel save(@RequestBody @Valid UserRequest userRequest) throws Exception {
    return userMapper.toDto(userService.save(userMapper.fromDto(userRequest)));
  }

  @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
  @PutMapping("update/{id}")
  public UserViewModel update(@RequestBody @Valid UserRequest userRequest) {
    return userMapper.toDto(userService.update(userMapper.fromDto(userRequest)));
  }

  @PreAuthorize("hasRole('ADMIN')")
  @DeleteMapping("{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@RequestBody @Valid UserRequest userRequest) {
    userService.delete(userMapper.fromDto(userRequest));
  }
}
