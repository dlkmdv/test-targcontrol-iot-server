package com.targcontrol.iot.controllers;

import com.targcontrol.iot.exceptions.DeviceNotFoundException;
import com.targcontrol.iot.exceptions.DuplicatedEntryException;
import com.targcontrol.iot.exceptions.UserNotFoundException;
import com.targcontrol.iot.views.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class ControllerExceptionHandler {

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(value = DeviceNotFoundException.class)
  public ErrorMessage handleDeviceNotFoundException(DeviceNotFoundException e, WebRequest request) {
    return new ErrorMessage(e.getMessage());
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(value = UserNotFoundException.class)
  public ErrorMessage handleUserNotFoundException(UserNotFoundException e, WebRequest request) {
    return new ErrorMessage(e.getMessage());
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(value = DuplicatedEntryException.class)
  public ErrorMessage handleDuplicatedEntryException(DuplicatedEntryException e,
      WebRequest request) {
    return new ErrorMessage(e.getMessage());

  }
}

