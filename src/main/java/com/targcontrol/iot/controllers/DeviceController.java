package com.targcontrol.iot.controllers;

import com.targcontrol.iot.mappers.DeviceMapper;
import com.targcontrol.iot.models.Device;
import com.targcontrol.iot.services.DeviceService;
import com.targcontrol.iot.utils.TokenUtil;
import com.targcontrol.iot.views.DeviceRequest;
import com.targcontrol.iot.views.DeviceViewModel;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/device")
public class DeviceController {

  private final TokenUtil tokenUtil;
  private final DeviceService deviceService;
  private final DeviceMapper deviceMapper;

  @GetMapping("/{id}")
  public DeviceViewModel getById(@PathVariable UUID id) {
    UUID orgId = tokenUtil.getAuthOrgId();
    return deviceMapper.toDto(deviceService.findByIdAndOrg(id, orgId));
  }


  @GetMapping("/all")
  public List<DeviceViewModel> getAll() {
    UUID orgId = tokenUtil.getAuthOrgId();
    List<Device> devices = this.deviceService.findAllByOrg(orgId);
    return this.deviceMapper.toDto(devices);
  }


  @PostMapping("/create")
  @ResponseStatus(HttpStatus.CREATED)
  public DeviceViewModel createDevice(@RequestBody @Valid DeviceViewModel request) {
    Device device = this.deviceMapper.fromDto(request);
    return this.deviceMapper.toDto(this.deviceService.create(device));
  }

  @PutMapping("/update")
  @ResponseStatus(HttpStatus.OK)
  public DeviceViewModel updateDevice(@RequestBody @Valid DeviceRequest request) {
    Device device = this.deviceMapper.fromDto(request);
    if (device.getToken() == null) {
      device.setToken(Device.getNewDeviceToken());
    }
    device = this.deviceService.update(device);
    return this.deviceMapper.toDto(device);
  }

  @PostMapping("/delete")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteDevice(@RequestBody @Valid DeviceRequest request) {
    Device device = deviceMapper.fromDto(request);
    UUID orgId = tokenUtil.getAuthOrgId();
    device.setOrganizationId(orgId);
    deviceService.delete(device);
  }


}
