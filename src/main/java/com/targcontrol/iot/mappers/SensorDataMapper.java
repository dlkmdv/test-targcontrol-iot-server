package com.targcontrol.iot.mappers;

import com.targcontrol.iot.models.SensorData;
import com.targcontrol.iot.views.SensorDataViewModel;
import java.util.List;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SensorDataMapper {

    SensorDataViewModel toDto(SensorData content);
    SensorData fromDto(SensorDataViewModel content);

    List<SensorDataViewModel> toDto(List<SensorData> content);
    List<SensorData> fromDto(List<SensorDataViewModel> content);
}
