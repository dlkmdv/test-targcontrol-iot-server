package com.targcontrol.iot.mappers;

import com.targcontrol.iot.models.User;
import com.targcontrol.iot.views.UserRequest;
import com.targcontrol.iot.views.UserViewModel;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {

  @Mapping(source = "enable", target = "enable")
  UserViewModel toDto(User content);

  @Mapping(source = "enable", target = "enable")
  User fromDto(UserRequest content);

  List<UserViewModel> toDto(List<User> content);

  List<User> fromDto(List<UserRequest> content);
}
