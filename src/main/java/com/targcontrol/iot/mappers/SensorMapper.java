package com.targcontrol.iot.mappers;

import com.targcontrol.iot.models.Sensor;
import com.targcontrol.iot.views.SensorViewModel;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SensorMapper {

  SensorViewModel toDto(Sensor content);

  Sensor fromDto(SensorViewModel content);

  List<SensorViewModel> toDto(List<Sensor> content);

  List<Sensor> fromDto(List<SensorViewModel> content);
}
