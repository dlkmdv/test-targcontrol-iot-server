package com.targcontrol.iot.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = Sensor.TABLE_NAME, schema = Sensor.SCHEMA_NAME)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Sensor {

  public static final String SCHEMA_NAME = "targcontrol_iot";
  public static final String TABLE_NAME = "sensor";

  @Id
  @Column(name = "id")
  private UUID id;

  @Column(name = "organization_id")
  private UUID organizationId;

  @Column(name = "name")
  private String name;

  @ManyToOne
  @EqualsAndHashCode.Exclude
  @ToString.Exclude
  @JoinColumn(name = "device_id", nullable = false)
  private Device device;

}
