package com.targcontrol.iot.models;

public enum Role {
    ROLE_USER, ROLE_ADMIN
}
