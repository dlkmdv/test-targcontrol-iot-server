package com.targcontrol.iot.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = Device.TABLE_NAME, schema = Device.SCHEMA_NAME)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Device {

  public static final String SCHEMA_NAME = "targcontrol_iot";
  public static final String TABLE_NAME = "device";

  @Id
  @Column(name = "id")
  @GeneratedValue
  private UUID id;

  @Column(name = "organization_id")
  private UUID organizationId;

  @Column(name = "name")
  private String name;

  @Column(name = "token")
  private String token;

  @OneToMany(mappedBy = "device")
  @EqualsAndHashCode.Exclude
  @ToString.Exclude
  private Set<Sensor> sensors;

  @Column(name = "device_state")
  @Enumerated(EnumType.STRING)
  private DeviceState deviceState;

  public static String getNewDeviceToken() {
    return UUID.randomUUID().toString();
  }
}
