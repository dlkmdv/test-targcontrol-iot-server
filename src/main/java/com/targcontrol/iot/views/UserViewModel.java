package com.targcontrol.iot.views;

import com.targcontrol.iot.models.Role;
import java.util.UUID;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UserViewModel {

  private UUID id;

  @NotNull
  private String username;

  @NotNull
  @Email
  private String email;

  @NotNull
  private UUID organizationId;

  @NotNull
  private Role role;

  @NotNull
  private boolean enable;
}
