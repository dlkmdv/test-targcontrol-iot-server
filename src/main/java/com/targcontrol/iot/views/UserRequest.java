package com.targcontrol.iot.views;

import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class UserRequest extends UserViewModel {

  private UUID id;

  @NotNull
  private String password;
}
