package com.targcontrol.iot.repositories;

import com.targcontrol.iot.models.SensorData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

@Repository
public interface SensorDataRepository extends JpaRepository<SensorData, UUID> {

    List<SensorData> findAllByOrganizationIdAndDateTimeGreaterThanEqualAndDateTimeLessThanEqual(
            UUID organizationId,
            ZonedDateTime startDate,
            ZonedDateTime endDate);
}
