package com.targcontrol.iot.repositories;

import com.targcontrol.iot.models.User;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, UUID> {

  List<User> findAll();

  Optional<User> findById(UUID uuid);

  Optional<User> findByUsername(String username);

  Optional<User> findByEmail(String email);
}
