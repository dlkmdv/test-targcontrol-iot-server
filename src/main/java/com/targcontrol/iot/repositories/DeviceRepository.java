package com.targcontrol.iot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.targcontrol.iot.models.Device;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface DeviceRepository extends JpaRepository<Device, UUID> {

    Optional<Device> findByIdAndOrganizationId (UUID id, UUID organizationId);

    List<Device> findAllByOrganizationId(UUID organizationId);

    Optional<Device> findByToken (String token);

}
